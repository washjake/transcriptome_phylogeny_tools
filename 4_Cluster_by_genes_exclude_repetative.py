from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import sys
import os

EXCLUDE_REPETATIVE_GENES=True
taxon_cut_off = sys.argv[1]
count= len(sys.argv)
gene_cluster_DIR="gene_clusters/"
if EXCLUDE_REPETATIVE_GENES==True:
	gene_cluster_DIR="gene_clusters_repetative_genes_excluded/"
os.system("mkdir "+gene_cluster_DIR)
repeat_list=[]
#print count
for x in range(2,count):
	#print x
	#sed_cmd1 = "sed -i -e 's/>.*@/>/g'"+sys.argv[x]
	#print (sed_cmd1)
	#os.system(sed_cmd1)
	repeats={}
	for record1 in SeqIO.parse(sys.argv[x], "fasta"):
		if record1.id.split("+")[0] not in repeats:
			repeats[record1.id.split("+")[0]]=0
		else:
			repeats[record1.id.split("+")[0]]+=1
			repeat_list.append(sys.argv[x]+"_"+record1.id)
	#print repeats
	for record in SeqIO.parse(sys.argv[x], "fasta"):
		output_handle = open(gene_cluster_DIR+record.id.split("@")[1].split("+")[0]+".fasta", "a")
		if repeats[record.id.split("+")[0]]==0:
			Species_added_to_anotation = SeqRecord(record.seq, id = record.id.split("@")[0], description = record.description)
		else:
			if EXCLUDE_REPETATIVE_GENES: continue
		SeqIO.write(Species_added_to_anotation, output_handle, "fasta")
	#print repeats
output_handle.close()
#print repeat_list
# remove clusters with less taxa than cut off
for i in os.listdir(gene_cluster_DIR):
	if i[len(i)-6:] != ".fasta": continue
	seq_count = 0
	taxa=[]
	for record in SeqIO.parse(gene_cluster_DIR+i, "fasta"):
		if record.id not in taxa:
			taxa.append(record.id)
	if len(taxa) < int(taxon_cut_off):
		cmd = "rm "+gene_cluster_DIR+i
		print (cmd)
		os.system(cmd)
#print taxa
