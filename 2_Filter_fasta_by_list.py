from Bio import SeqIO
#from Bio.Seq import Seq
#from Bio.SeqRecord import SeqRecord
import os, sys

fasta_file = SeqIO.parse(sys.argv[1], "fasta")
list = open(sys.argv[2],"Ur")
outfile = open(sys.argv[1]+"_single_copy_genes_only.fasta","w")

SCgene_names = []
for line in list:
	if line[:5]=="Block": continue
	#print line
	#print(line.split("\t")[1].split(".v2.1")[0])
	SCgene_names.append(line.split("\t")[1].split(".v2.1")[0])

count = 0
for record in fasta_file:
	#print(record.id.split("@")[1].split(".p")[0])
	if record.id.split(".p")[0] in SCgene_names:
		count +=1
		SeqIO.write(record, outfile, "fasta")

print(str(count)+" single copy genes in list and fasta file.")