import os, sys

b_g_file= open(sys.argv[1],"rU") #file with syntenic blocks and their corresponding genes
align_dir= sys.argv[2] #input directory with genetrees in it
out_dir= sys.argv[3] #output directory
genes_per_block=sys.argv[4]

b_g_dictionary={}
tot_genes=0
for line in b_g_file:
	if line[:5]=="Block": continue
	b_g_dictionary[line.split("\t")[1].split(".v2.1")[0]]=line.split("\t")[0]

os.system("mkdir "+out_dir)
count_dict={}
for file in os.listdir(align_dir):
	if file[len(file)-8:len(file)]==".aln-cln":
		try: 
			os.system("mkdir "+out_dir+"/"+b_g_dictionary["Sobic."+file.split(".fasta.aln-cln")[0].split("Sobic.")[1]]+"/")
			cmd="cp "+align_dir+"/"+file+" "+out_dir+"/"+b_g_dictionary["Sobic."+file.split(".fasta.aln-cln")[0].split("Sobic.")[1]]+"/"
		except: continue
		print cmd
		os.system(cmd)
		#print "Sobic."+file.split(".fasta.aln-cln")[0].split("Sobic.")[1]
		try:count_dict[b_g_dictionary["Sobic."+file.split(".fasta.aln-cln")[0].split("Sobic.")[1]]]+=1
		except:count_dict[b_g_dictionary["Sobic."+file.split(".fasta.aln-cln")[0].split("Sobic.")[1]]]=1
#print count_dict
print ("block\tNumberOfGenes")
count=0
for b in sorted(count_dict.keys()):
	if count_dict[b] < int(genes_per_block):
		cmd2="rm -r "+out_dir+"/"+str(b)
		print cmd2
		os.system(cmd2)
		continue
	print (str(b)+"\t"+str(count_dict[b]))
	count+=1
print str(count)+" blocks with at least "+str(genes_per_block)+" gene(s)."