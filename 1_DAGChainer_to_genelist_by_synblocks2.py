import os, sys

#Use Merged or unmegred DAG from COGE

infile = open(sys.argv[1],"rU")
out_list = open(sys.argv[1]+"_out_list.txt","w")
out_block_stats = open(sys.argv[1]+"_out_block_stats.txt","w")

block_num=0
total_genes=0
num_genes_in_block=0
genes_in_block=[]
block=""
max=0
min=""

for line in infile:
	if line[:3]=="###":
		if block_num==0: 
			out_block_stats.write("Block\tGenes\n")
			out_list.write("Block\tGene\n")
		if block_num!=0:
			out_block_stats.write(block+"\t"+str(num_genes_in_block)+"\n")
			for gene in genes_in_block:
				cmd = block+"\t"+gene
				out_list.write(cmd+"\n")
		block_num+=1
		num_genes_in_block=0
		genes_in_block=[]
		max=0
		min=""
		continue
	if line[:1]=="#": continue
	num_genes_in_block+=1
	total_genes+=1
	if int(line.split("\t")[5].split("||")[1]) < min: min=int(line.split("\t")[5].split("||")[1])
	if int(line.split("\t")[5].split("||")[2]) > max: max=int(line.split("\t")[5].split("||")[2])
	block="Chr"+line.split("\t")[5].split("||")[0]+"_"+str(min)+"-"+str(max)+"_"+str(block_num)
	genes_in_block.append(line.split("\t")[5].split("||")[3])

out_block_stats.write(block+"\t"+str(num_genes_in_block)+"\n")
for gene in genes_in_block:
	cmd = block+"\t"+gene
	out_list.write(cmd+"\n")
print (str(total_genes)+" Total CDS")
print (str(block_num)+" Total Syntenic Blocks")
out_block_stats.close()
out_list.close()