import os, sys

#Use Merged or unmegred DAG from COGE

infile = open(sys.argv[1],"rU")
out_list = open(sys.argv[1]+"_out_list.txt","w")
out_block_stats = open(sys.argv[1]+"_out_block_stats.txt","w")

block_num=0
total_genes=0
genes_in_block=0
block=""

for line in infile:
	if line[:3]=="###":
		if block_num==0: 
			out_block_stats.write("Block\tGenes\n")
			out_list.write("Block\tGene\n")
		if block_num!=0: out_block_stats.write(block+"\t"+str(genes_in_block)+"\n")
		block_num+=1
		genes_in_block=0
		continue
	if line=="":
		out_block_stats.write(block+"\t"+str(genes_in_block)+"\n")
		continue
	if line[:1]=="#": continue
	genes_in_block+=1
	total_genes+=1
	block="Chr"+line.split("\t")[5].split("||")[0]+"_"+str(block_num)
	cmd = "Chr"+line.split("\t")[5].split("||")[0]+"_"+str(block_num)+"\t"+line.split("\t")[5].split("||")[3]
	out_list.write(cmd+"\n")
	#print cmd
print (str(total_genes)+" Total CDS")
print (str(block_num)+" Total Syntenic Blocks")
out_block_stats.close()
out_list.close()