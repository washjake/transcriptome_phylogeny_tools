from Bio import SeqIO
import os, sys
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

evalue="0.0001" #"0.00001"
id_filter=int(sys.argv[4]) #75 #85
ref_file= sys.argv[1]
trans_fa= sys.argv[2]
seq_type=sys.argv[3] #aa or dna
files_in_DIR=[]

for file in os.listdir("."):
	files_in_DIR.append(file)

if "AA_blast_database.fasta.psq" in files_in_DIR or "DNA_blast_database.fasta" in files_in_DIR:
	print("Using previously created database")
else:
	print("Making blast database from reference file")
	if seq_type=="aa": mbdb = "makeblastdb -in "+ref_file+" -parse_seqids -dbtype prot -out AA_blast_database.fasta"
	if seq_type=="dna": mbdb = "makeblastdb -in "+ref_file+" -parse_seqids -dbtype nucl -out DNA_blast_database.fasta"
	print mbdb
	os.system(mbdb)

print "Running blast analysis"
if seq_type=="aa":run_blast = "blastp -db AA_blast_database.fasta -query "+trans_fa+" -evalue "+evalue+" -out "+trans_fa+"_sseqid_evalue_bitscore_score_pident_qseqid_sstart_send_qstart_qend -outfmt '6 sseqid evalue bitscore score pident qseqid sstart send qstart qend'"
if seq_type=="dna":run_blast = "blastn -db DNA_blast_database.fasta -query "+trans_fa+" -evalue "+evalue+" -out "+trans_fa+"_sseqid_evalue_bitscore_score_pident_qseqid_sstart_send_qstart_qend -outfmt '6 sseqid evalue bitscore score pident qseqid sstart send qstart qend'"
print run_blast
os.system(run_blast)

print "Sorting blast results and selecting best hit for each contig"
sort = "sort -k6,6 -k5,5gr "+trans_fa+"_sseqid_evalue_bitscore_score_pident_qseqid_sstart_send_qstart_qend | sort -u -k6,6 | sort -k1,1 > "+trans_fa+"_sseqid_evalue_bitscore_score_pident_qseqid_sstart_send_qstart_qend_SORTED_BEST_HIT.txt"
print sort
os.system(sort)

print "Discarding results with less than "+str(id_filter)+"% identity."
blast_output_sorted = open(trans_fa+"_sseqid_evalue_bitscore_score_pident_qseqid_sstart_send_qstart_qend_SORTED_BEST_HIT.txt","rU")
filtered_blast_85_out = open(trans_fa+"_filtered_blast_85.txt","w")
for line in blast_output_sorted:
	if len(line) < 3: continue #skip empty lines
	if float(line.split("\t")[4]) >= id_filter:
		filtered_blast_85_out.write(line)
filtered_blast_85_out.close()

print("Creating sequential annotations for duplicate genes")
Annotation={}
dup_record={}
copy_num=1
prev_ann=""
filtered_blast_85 = open(trans_fa+"_filtered_blast_85.txt","rU")
for line in filtered_blast_85:
	if len(line) < 3: continue #skip empty lines
	if line.split("\t")[0] == prev_ann:
		copy_num+=1
		dup_record[line.split("\t")[0]]=copy_num
		Annotation[line.split("\t")[5]]=line.split("\t")[0]+"+"+str(copy_num)
	else:
		Annotation[line.split("\t")[5]]=line.split("\t")[0]
		copy_num=1
	prev_ann=line.split("\t")[0]

print "Writing annotated homolog list"
count = 0
outfile=open(trans_fa+"_homologs.fasta","w")
for record in SeqIO.parse(trans_fa,"fasta"):
	#print(record.id.split("@")[1].split(".p")[0])
	try:
		test=Annotation[record.id]
	except KeyError: continue
	count+=1
	Anotation_name=record.id.split("@")[0]+"@"+Annotation[record.id]
	#gene_out = SeqRecord(record.seq,id=Anotation_name,description=record.id)
	outfile.write(">"+Anotation_name+" "+record.id+"\n"+str(record.seq)+"\n")
	#SeqIO.write(gene_out, outfile, "fasta")

dup_stats=open(trans_fa+"_duplicate_gene_stats.txt","w")
for i in dup_record:
	dup_stats.write(i+"\t"+str(dup_record[i])+"\n")
print dup_record
print(str(count)+" homologs annotated and writen to "+trans_fa+"_homologs.fasta")

os.system("rm "+trans_fa+"_sseqid*")
os.system("rm "+trans_fa+"_filtered_blast_85.txt")
