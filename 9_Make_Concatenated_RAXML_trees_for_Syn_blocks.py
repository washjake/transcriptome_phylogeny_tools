#before running this script be sure to change concatenate_matrices script to read 'MATRIX_FILE_HEADING = "Sobic"'

import os,sys

cores=sys.argv[2]

syn_bl_dirs={} #dictionary variable with syn blocks directory names as key, number of trees in each as value
for dir in os.listdir(sys.argv[1]):
	for tree in os.listdir(sys.argv[1]+"/"+dir):
		if tree[:6]=="Sobic.": 
			try:syn_bl_dirs[dir]+=1
			except:syn_bl_dirs[dir]=1

os.system("mkdir "+sys.argv[1]+"/con_raxml_trees")
for dir1 in syn_bl_dirs:
	if syn_bl_dirs[dir1]> 1:
		cmd1 = "python concatenate_matrices.py "+sys.argv[1]+"/"+dir1+"/ 30 1 aa "+sys.argv[1]+"/"+dir1+"/"+dir1+"_con"
		print cmd1
		os.system(cmd1)
		cmd2 = "raxmlHPC-PTHREADS-SSE3 -T "+cores+" -f a -x 12345 -# 200 -p 12345 -m PROTCATWAG -s "+sys.argv[1]+"/"+dir1+"/"+dir1+"_con.phy -q "+sys.argv[1]+"/"+dir1+"/"+dir1+"_con.model -n "+dir1+"_con -w "+os.path.abspath(sys.argv[1]+"/con_raxml_trees")+" -o Sbicolor_255_v2.1"
		print cmd2
		os.system(cmd2)
